characters_list = ['Barack Obama',
                   'Angela Merkel',
                   'Olaf Scholz',
                   'Elon Musk',
                   'Bill Gates',
                   'Joe Biden',
                   'Alexei Navalny',
                   'Timothy Cook',
                   'Britney Spears',
                   'Jim Carrey',
                   'Keanu Reeves',
                   'Johny Depp',
                   'Leonardo DiCaprio',
                   'Jeffrey Bezos',
                   'Angelina Jolie',
                   'William Bradley Pitt']

from bs4 import BeautifulSoup
import requests
import re
import urllib3
import os
import argparse
import sys
import json

# adapted from http://stackoverflow.com/questions/20716842/python-download-images-from-google-image-search
http = urllib3.PoolManager()
def get_soup(url, header):
    return BeautifulSoup(http.request(url=url, method='GET', headers=header).data, 'html.parser')


def main(args):
    parser = argparse.ArgumentParser(description='Scrape Google images')
    parser.add_argument('-s', '--search', default='bananas', type=str, help='search term')
    parser.add_argument('-n', '--num_images', default=10, type=int, help='num images to save')
    parser.add_argument('-d', '--directory', default='/Users/luizashishina/Downloads/images', type=str, help='save directory')
    args = parser.parse_args()
    query = args.search  # raw_input(args.search)
    max_images = args.num_images
    save_directory = args.directory
    image_type = "Action"
    query = query.split()
    query = '+'.join(query)
    url = "https://www.google.com/search?q=" + query + "&source=lnms&tbm=isch"
    header = {
        'User-Agent': "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36"}
    soup = get_soup(url, header)
    ActualImages = []  # contains the link for Large original images, type of  image
    for a in soup.find_all("img", {"class": "yWs4tf"}):
        link = a.attrs['src']
        ActualImages.append(link)
    for i, img in enumerate(ActualImages[0:max_images]):
        try:
            raw_img = http.request(url=img, headers=header, method='GET')
            directory = save_directory
            if not os.path.exists(directory):
                os.makedirs(directory)
            f = open(os.path.join(directory, query + "_" + str(i) + ".jpg"), 'wb')

            f.write(raw_img.data)
            f.close()
        except Exception as e:
            print("could not load : " + img)
            print(e)


main(sys.argv)
